from email.headerregistry import Address
import ipaddress


def validate_ip_address(address):
    parts = address.split(".")
    # print(parts)

    
    if len(parts) != 4:
        print("IP address {} is not valid".format(address))
        return False

    for part in parts:
        if not isinstance(int(part), int):
            print("IP address {} is not valid".format(address))
            return False

        if int(part) < 0 or int(part) > 255:
            print("IP address {} is not valid".format(address))
            return False
 
    print("IP address {} is valid".format(address))
    return True





if __name__ == "__main__":
    address="192.168.1.1"
    validate_ip_address(address)