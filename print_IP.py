from pathlib import Path


def print_IP(filename):
    file=(Path(filename).read_text())
    ip_array = file.split('\n')
    for index, ip in enumerate(ip_array):
        print(f"{index}. {ip}")
    return ip_array


if __name__ == "__main__":
    filename="C:/Users/netan/OneDrive/שולחן העבודה/Linux/list.txt"
    print_IP(filename)