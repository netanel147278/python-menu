from statistics import mean
from time import sleep
from urllib.error import URLError
from search_IP import search_IP
from add_IP import add_IP
from delete_IP import delete_IP
from print_IP import print_IP
from search_URL import search_URL
from add_URL import add_URL
from delete_URL import delete_URL
from update_URL import update_URL
from print_URL import print_URL


my_dict={"www.google.com":"8.8.8.8" , "www.ynet.com":"10.10.10.10"}
# TODO: change to switch case 
def menu(filename):
    while(True):
        choise1=input("MENU\n--------------\na.Ip system?\nb.DNS system?\n")
        if(choise1=="a"):
            choise2=input("\nMenu IP system\n-----------\n1.Search for IP addres from a list\n2.add IP addres to a list\n3.Delete IP addres from a list\n4.Print all the Ip  to the screen\n")
            if(choise2=="1"):
                search_IP(filename)
            elif(choise2=="2"):
                add_IP(filename)
            elif(choise2=="3"):
                delete_IP(filename)
            elif(choise2=="4"):
                print_IP(filename)
            else:
                print("Only 1-4!!")
        elif(choise1=="b"):
            choise3=input("\nMenu DNS system\n-----------\n1.Search for URL from a dictionary\n2.add URL + IP address to a dictionary\n3.Delete URl from a dictionary\n4.Update the IP address of specific URl\n5.Print all the URL to the screen\n")
            if(choise3=="1"):
                search_URL(my_dict)
            elif(choise3=="2"):
                add_URL(my_dict)
            elif(choise3=="3"):
                delete_URL(my_dict)
            elif(choise3=="4"):
                update_URL(my_dict)
            elif(choise3=="5"):
                print_URL(my_dict)
            else:
                print("Only 1-5!!")
        else:
            print("Only a or b")
        exit=input("Do you want to exit? y/n\n")
        if(exit=="yes" or exit=="y"):
            print("Bye Bye")
            break


if __name__ == "__main__":
    filename="C:/Users/netan/OneDrive/שולחן העבודה/Linux/list.txt"
    menu(filename)